import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TopicsHomeComponent } from './topics-home.component';

describe('TopicsHomeComponent', () => {
  let component: TopicsHomeComponent;
  let fixture: ComponentFixture<TopicsHomeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TopicsHomeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TopicsHomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
