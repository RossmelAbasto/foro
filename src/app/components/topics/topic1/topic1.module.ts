import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'src/app/components/shared/shared.module';
import { DiscussionsComponent } from './discussions/discussions.component';
import { AddDiscussionComponent } from './discussions/add-discussion/add-discussion.component';
import { SeeDiscussionsComponent } from './discussions/see-discussion/see-discussion.component';

@NgModule({
    declarations: [
        DiscussionsComponent,
        AddDiscussionComponent,
        SeeDiscussionsComponent
    ],
    imports: [
      CommonModule,
      SharedModule
    ]
  })
  export class Topics1Module { }
  