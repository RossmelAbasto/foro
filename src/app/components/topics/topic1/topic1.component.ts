import { Component, OnInit } from '@angular/core';
import { ICommentsTopic1 } from '../../../interface/comments';
import { CommentsService } from '../../../service/comments.service';
import { MatTableDataSource } from '@angular/material/table';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';

@Component({
  selector: 'app-topic1',
  templateUrl: './topic1.component.html',
  styleUrls: ['./topic1.component.scss']
})
export class Topic1Component implements OnInit {
  CommentList1: ICommentsTopic1[] = [];

  //Para las columnas
  displayedColumns: string[] = ['title', 'acciones'];
  dataSource!: MatTableDataSource<any>;

  //Para el paginator
  // @ViewChild(MatPaginator) paginator!: MatPaginator;

  constructor(
    private _commentService: CommentsService,
    private _snackbar: MatSnackBar,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.loadComments();
  }

  loadComments() {
    this.CommentList1 = this._commentService.getComments();
    this.dataSource = new MatTableDataSource(this.CommentList1);
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  eliminarCita(comment: string) {
    const opcion = confirm(`${comment} will be deleted, continue?`);

    if (opcion) {
      console.log(comment);
      this._commentService.deleteComment(comment);
      this.loadComments();
      this._snackbar.open('Discussion was deleted succesfully', 'Accept', {
        duration: 1500,
        horizontalPosition: 'center',
        verticalPosition: 'bottom',
      });
    }
  }

  modifyComment(title: string) {
    console.log(title);
    this.router.navigate(['/add-discussion', title]);
  }

  seeComment(title: string): void {
    console.log(title);
    this.router.navigate(['/ver-comment', title]);
  }
}
