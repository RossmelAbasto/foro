import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { CommentsService } from '../../../../../service/comments.service';
@Component({
  selector: 'app-ver-receta',
  templateUrl: './see-discussion.component.html',
  styleUrls: ['./see-discussion.component.scss'],
})
export class SeeDiscussionsComponent implements OnInit {
  form!: FormGroup;
  constructor(
    private fb: FormBuilder,
    private _commentService: CommentsService,
    private router: Router,
    private activateRouted: ActivatedRoute
  ) {
    this.activateRouted.params.subscribe((params) => {
      const id = params['id'];
      console.log(id);

      const title = this._commentService.searchComment(id);
      console.log(title);

      if (Object.keys(title).length === 0) {
        // volvemos a la ruta usuarios si no hay datos en el json
        this.router.navigate(['/agenda']);
      }

      this.form = this.fb.group({
        title: [
          '',
          [
            Validators.required,
            Validators.minLength(4),
            Validators.pattern(/^[a-zA-zñÑ\s]+$/),
          ],
        ],
        // apellido:['',[Validators.required,Validators.minLength(4),Validators.pattern(/^[a-zA-zñÑ\s]+$/)]],
        // correo:['',[Validators.required,Validators.pattern(/^(([^<>()\[\]\.,;:\s@\”]+(\.[^<>()\[\]\.,;:\s@\”]+)*)|(\”.+\”))@(([^<>()[\]\.,;:\s@\”]+\.)+[^<>()[\]\.,;:\s@\”]{2,})$/)]],
        // celular:['',[Validators.required,Validators.pattern(/^([0-9])*$/)]],
        // fecha:['',[Validators.required]],
        // hora:['',[Validators.required]],
        description: ['', [Validators.required]],
      });

      this.form.patchValue({
        title: title.title,
        // apellido:title.apellido,
        // correo:title.correo,
        // celular:title.celular,
        // fecha:title.fecha,
        // hora:title.hora,
        description: title.description,
      });
    });
  }
  ngOnInit(): void {}

  Volver(): void {
    this.router.navigate(['/agenda']);
  }
}
