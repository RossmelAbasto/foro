import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SeeDiscussionComponent } from './see-discussion.component';

describe('SeeDiscussionComponent', () => {
  let component: SeeDiscussionComponent;
  let fixture: ComponentFixture<SeeDiscussionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SeeDiscussionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SeeDiscussionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
