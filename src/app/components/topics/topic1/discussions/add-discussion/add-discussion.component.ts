import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { ICommentsTopic1 } from '../../../../../interface/comments';
import { CommentsService } from '../../../../../service/comments.service';

@Component({
  selector: 'app-add-discussion',
  templateUrl: './add-discussion.component.html',
  styleUrls: ['./add-discussion.component.scss'],
})
export class AddDiscussionComponent implements OnInit {
  form!: FormGroup;
  // PARA modificar  y cambiar
  adicionar: boolean = true;
  titulo = 'ADD DISCUSSIONS';

  constructor(
    private fb: FormBuilder,
    private _commentService: CommentsService,
    private router: Router,
    private _snackbar: MatSnackBar,
    private activateRouted: ActivatedRoute
  ) {
    this.activateRouted.params.subscribe((params) => {
      const id = params['id'];
      console.log(id);

      this.form = this.fb.group({
        title: [
          '',
          [
            Validators.required,
            Validators.minLength(4),
            Validators.pattern(/^[a-zA-zñÑ\s]+$/),
          ],
        ],
        //  apellido:['',[Validators.required,Validators.minLength(4),Validators.pattern(/^[a-zA-zñÑ\s]+$/)]],
        //  correo:['',[Validators.required,Validators.pattern(/^(([^<>()\[\]\.,;:\s@\”]+(\.[^<>()\[\]\.,;:\s@\”]+)*)|(\”.+\”))@(([^<>()[\]\.,;:\s@\”]+\.)+[^<>()[\]\.,;:\s@\”]{2,})$/)]],
        //  celular:['',[Validators.required,Validators.pattern(/^([0-9])*$/)]],
        //  fecha:['',[Validators.required]],
        //  hora:['',[Validators.required]],
        description: ['', [Validators.required]],
      });

      if (id != 'nuevo') {
        const title = _commentService.searchComment(id);
        console.log(title);
        if (Object.keys(title).length === 0) {
          this.router.navigate(['/agenda']);
        }
        this.form.patchValue({
          title: title.title,
          description: title.description,
        });
        this.adicionar = false;
        this.titulo = 'MODIFY DISCUSSIONS';
      }
    });
  }

  ngOnInit(): void {}

  addComments(): void {
    if (!this.form.valid) {
      return;
    }

    console.log(this.form.value);

    const comment: ICommentsTopic1 = {
      title: this.form.value.title,
      description: '',
    };
    console.log(comment);
    // Para modificar
    if (this.adicionar) {
      this._commentService.addComment(comment);
      //redireccionamos a la tabla de agenda
      this.router.navigate(['/agenda']);

      this._snackbar.open('La Receta fue agregada con exito', 'Aceptar', {
        duration: 1500,
        horizontalPosition: 'center',
        verticalPosition: 'bottom',
      });
    } else {
      this._commentService.modifyComment(comment);
      this.router.navigate(['/agenda']);

      this._snackbar.open('La Receta fue modificada con exito', 'Aceptar', {
        duration: 1500,
        horizontalPosition: 'center',
        verticalPosition: 'bottom',
      });
    }
  }

  get NombreNoValido() {
    return this.form.get('title')?.invalid && this.form.get('title')?.touched;
  }

  Volver(): void {
    this.router.navigate(['/agenda']);
  }
}
