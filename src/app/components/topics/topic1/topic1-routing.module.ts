import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AddDiscussionComponent } from './discussions/add-discussion/add-discussion.component';
import { SeeDiscussionsComponent } from './discussions/see-discussion/see-discussion.component';
import { Topic1Component } from './topic1.component';

const routes: Routes = [
    { path: '', component: Topic1Component },
    { path: 'add-discussions', component: AddDiscussionComponent },
    { path: 'see-discussions', component: SeeDiscussionsComponent },
    { path: '**', redirectTo: '/', pathMatch: 'full' }
  ];
  
  @NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
  })
  export class Topics1RoutingModule { }
  