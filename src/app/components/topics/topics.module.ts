import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'src/app/components/shared/shared.module';


import { TopicsRoutingModule } from './topics-routing.module';
import { Topic1Component } from './topic1/topic1.component';
import { Topic2Component } from './topic2/topic2.component';
import { Topic3Component } from './topic3/topic3.component';
import { TopicsHomeComponent } from './topics-home/topics-home.component';


@NgModule({
  declarations: [
    TopicsHomeComponent,
    Topic1Component,
    Topic2Component,
    Topic3Component,
  ],
  imports: [
    CommonModule,
    TopicsRoutingModule,
    SharedModule
  ]
})
export class TopicsModule { }
