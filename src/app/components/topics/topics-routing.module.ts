import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { TopicsHomeComponent } from './topics-home/topics-home.component';
import { Topic1Component } from './topic1/topic1.component';
import { Topic2Component } from './topic2/topic2.component';
import { Topic3Component } from './topic3/topic3.component';

const routes: Routes = [
  { path: '', component: TopicsHomeComponent },
  { path: 'topic1', component: Topic1Component },
  { path: 'topic2', component: Topic2Component },
  { path: 'topic3', component: Topic3Component },
  { path: '**', redirectTo: '/', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TopicsRoutingModule { }
