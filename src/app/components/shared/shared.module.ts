import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SpinnerComponent } from './spinner/spinner.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import {MatIconModule} from '@angular/material/icon';
import { MatTableModule } from '@angular/material/table'

@NgModule({
  declarations: [SpinnerComponent],
  imports: [CommonModule, FormsModule, ReactiveFormsModule, MatFormFieldModule],
  exports: [
    FormsModule,
    ReactiveFormsModule,
    SpinnerComponent,
    MatFormFieldModule,
    MatIconModule,
    MatTableModule
  ],
})
export class SharedModule {}
