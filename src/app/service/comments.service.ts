import { Injectable } from '@angular/core';
import { elementAt } from 'rxjs';
import { ICommentsTopic1 } from '../interface/comments';

@Injectable({
  providedIn: 'root'
})
export class CommentsService {
  CommentList1: ICommentsTopic1[] = [];
  constructor() { }

  getComments(){
    if(localStorage.getItem('commentsLis')===null){
      return this.CommentList1;
    }else{
      this.CommentList1=JSON.parse(localStorage.getItem('commentsLis')||"[]")
      return this.CommentList1
  }
  }
  addComment(comment:ICommentsTopic1){
    this.CommentList1.push(comment);
    let commentLis: ICommentsTopic1[] = [];
    if(localStorage.getItem('commentsLis')===null){
      commentLis.push(comment)
      localStorage.setItem('commentsLis',JSON.stringify(commentLis))
    }
    else{
      commentLis=JSON.parse(localStorage.getItem('commentsLis') || "[]")
      commentLis.push(comment);
      localStorage.setItem('commentsLis',JSON.stringify(commentLis));
  }
  }
  deleteComment(title:string){
    this.CommentList1=this.CommentList1.filter(data => data.title !== title)
    console.log(this.CommentList1);
    localStorage.setItem('commentsLis',JSON.stringify(this.CommentList1));
    this.getComments();
    
   
  }


   modifyComment(names:ICommentsTopic1){
    this.deleteComment(names.title);
    this.addComment(names);
  }

  searchComment(id:string):ICommentsTopic1{
    return this.CommentList1.find(element => element.title === id) || {} as ICommentsTopic1;
  }
}
